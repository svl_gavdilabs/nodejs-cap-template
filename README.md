# NodeJS CAP Template 

Simple NodeJS CAP template with extra templates for adding MTA support and cross-app routing.

Be aware that some of the files contain placeholder names, which should we changed to your actual project's name. 

These files include:

- package.json -> Change script endpoints and project name
- template-service.cds -> Change filename
- app/placeholder.txt -> Remove file

## Yeoman Scaffolding
Want the process automated and have the project setup with the correct names from the beginning?

Try out the [Yeoman CAP NodeJS generator](https://bitbucket.org/svl_gavdilabs/generator-cap-nodejs)