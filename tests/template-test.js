//Our testing framework
const chai = require("chai");
const chaiHttp = require("chai-http");

//Our testing server
const server = require("./server");

//For private function testing
const rewire = require("rewire");

//Preparing doc for tests
chai.use(chaiHttp);
chai.should();

let app = null;

before((done) => {
    server.then((result) => {
        app = result;
        done();
    });
});


//Example test
describe("Example functionality", () =>{   
    it("+ Should do something", () =>{
        const template = true;
        expect(template).to.be.equal(true);
    });
});