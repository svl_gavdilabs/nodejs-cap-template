# Templates - For What & Why? 
In order to ease the process of development for CF CAP, I've made these quick small template files that can be tossed into your project during development. 

There is currently 3 template files you can use. 

- ***mta.yaml*** is placed in the root folder if you want the project to be a multi-target app. 
- ***default-env.json*** is placed in the app folder if you wish to create a freestyle front-end with a routing between the backend and front-end.
- ***xs-app.json*** should also be placed inside the app folder alongside *default-env.json* to help with the routing.